'''
Created on 07/11/2020

@author: rafael
'''
class Nodo:
    def __init__(self, dato=None):
            self.dato = dato
            self.siguiente = None
            self.anterior = None

    def __str__(self):
        return f"Nodo=[Dato {self.dato}]"
        
class ListaDoblemeteEnlazada:
    def __init__(self):
        self.nodoInicio =Nodo()
        self.nodoFin =Nodo()
        self.nodoInicio.siguiente = self.nodoFin
        self.nodoFin.anterior = self.nodoInicio

    def verificarVacia(self):
        return self.nodoInicio.siguiente==self.nodoFin
    
    def agregarInicio(self, dato):
        self.agregarEntre(self.nodoInicio, Nodo(dato), self.nodoInicio.siguiente)
        return True
    
    def agregarFin(self, dato):
        self.agregarEntre(self.nodoFin.anterior,Nodo(dato), self.nodoFin)
        return True
    
    def agregarEntre(self, primero, nuevo, ultimo):
        primero.siguiente = nuevo
        nuevo.anterior = primero
        nuevo.siguiente = ultimo
        ultimo.anterior = nuevo
    
    def mostrarElementos(self):
        if self.verificarVacia():
            print("La lista esta vacia")
        else:
            temporal = self.nodoInicio.siguiente

            while(temporal!=self.nodoFin):
                print(f"<-{temporal}->", end="")
                temporal=temporal.siguiente
        print()
    
    def eliminarInicio(self):
        if self.verificarVacia():
            print("Lista Vacia")
            return None
        else:
            eliminado = self.nodoInicio.siguiente
            
            self.nodoInicio.siguiente = self.nodoInicio.siguiente.siguiente
            self.nodoInicio.siguiente.anterior = self.nodoInicio
            return eliminado

    def eliminarFin(self):
        if self.verificarVacia():
            print("Lista Vacia")
            return None
        else:
            eliminado = self.nodoFin.anterior
            
            self.nodoFin.anterior = self.nodoFin.anterior.anterior
            self.nodoFin.anterior.siguiente = self.nodoFin
            return eliminado

    def eliminarElementoEspecifico(self, dato):#////////
        if self.verificarVacia():
            return -1
        else:
            eliminado = None

            if dato==self.nodoInicio.siguiente.dato:
                eliminado = self.nodoInicio.siguiente
                self.nodoInicio.siguiente = eliminado.siguiente
                eliminado.siguiente.anterior = self.nodoInicio
                return eliminado.dato
            elif dato==self.nodoFin.anterior.dato:
                eliminado = self.nodoFin.anterior
                self.nodoFin.anterior = eliminado.anterior
                eliminado.anterior.siguiente = self.nodoFin
                return eliminado
            else:
                anterior = None
                siguiente = self.nodoInicio.siguiente
                
                while siguiente!=self.nodoFin:
                    if dato==siguiente.dato:
                        eliminado = siguiente
                        eliminado.siguiente.anterior = anterior
                        anterior.siguiente = siguiente.siguiente
                        return eliminado
                        break
                    anterior = siguiente
                    siguiente = siguiente.siguiente

            print("El elemento que deseas eliminar no existe")
            return -1




#-------Menu---------
op=""
lista=ListaDoblemeteEnlazada()
while(op!="D"):
    
    print("Elige una opcion")
    print("A) Agregar Elemento")
    print("B) Elimiar elemento")
    print("C) Mostrar elementos")
    print("D) Salir")
    op=input("Elige una opcion: ").upper()
    if(op=="A"):
        dato=int(input("Ingresa el dato que deseas agregar: "))
        op2=""
        while(op2!="A" or op2!="B"):
            print(f"A) Agregar {dato} al inicio")
            print(f"B) Agregar {dato} al final")
            op2=input("Elige una opcion: ").upper()
            if(op2=="A"):
                lista.agregarInicio(dato)
                break
            elif(op2=="B"):
                lista.agregarFin(dato)
                break
        print("----------------------------")
    elif(op=="B"):
        opB=""
        while(not(opB=="1" or opB=="2" or opB=="3")):
            print("1-> Eliminar pocicion inicio")
            print("2-> Eliminar pocicion final")
            print("2-> Elimar dato especifico")
            opB=input()
            if(opB=="1"):
                lista.eliminarInicio()
            elif(opB=="2"):
                lista.eliminarFin()
            elif(opB=="3"):
                datoEliminar=int(input("Ingresa el dato que deseas aliminar: "))
                datoEliminado=lista.eliminarElementoEspecifico(datoEliminar)
                if(datoEliminado==-1):
                    print("No se pudo eliminar el dato")
                else:
                    print("El dato se elimino correctamente ")
            else:
                print("Error no eligiste una opcion correcta")
        print("-----------------------------")
    elif(op=="C"):
        
        lista.mostrarElementos()
        print("------------------------------")
    elif(op=="D"):
        print("Saliendo...")
    else:
        print("Elige una opcion correcta")
                
                
